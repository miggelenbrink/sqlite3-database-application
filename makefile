#choose which compiler to use
CXX = g++

#flags to pass to the compiler
	# -Wall = all warinings
	# -g = debugging information
CXXFLAGS = -Wall -g -Wextra -lpthread 

EXECUTABLE = dbInterface
SOURCES := ${wildcard *.cpp} \	
SOURCES := ${wildcard sourcefiles/*.cpp} 			
HEADERS := ${wildcard *.h} \		
HEADERS := ${wildcard sourcefiles/*.h}

OBJECTS := ${SOURCES:.cpp=.o}
OBJECTS := ${OBJECTS:.c=.o}

.PHONY: all
all: ${EXECUTABLE}

$(EXECUTABLE): $(OBJECTS) 
	$(CXX) $(CXXFLAGS) $(OBJECTS) -o $@ -lsqlite3

# Create a clean environment
.PHONY: clean
clean:
	$(RM) $(EXECUTABLE) $(OBJECTS)
