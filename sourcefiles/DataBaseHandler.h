#include <iostream>
#include <string>
#include "interfaceDataBase.h"
#include "menuDataBase.h"

class DataBaseHandler
{
public:
    DataBaseHandler(const std::string dbName);
    ~DataBaseHandler();

    int DBHandler();

    int selectHandler();
    int insertHandler();
    int updateHandler();
    int deleteHandler();


private:
    menuDataBase dbMenu;
    interfaceDataBase dbInterface;

};


