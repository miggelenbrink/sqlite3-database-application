#include "DataBaseHandler.h"

DataBaseHandler::DataBaseHandler(const std::string dbName): dbInterface{dbName}, dbMenu{}
{
}

DataBaseHandler::~DataBaseHandler()
{
}

int DataBaseHandler::DBHandler()
{
    
    dbMenu.clearscreen();

    /*select operation*/
    while (dbMenu.selectOperation() == 1)
    {
        std::cout << "non-valid value, try again" << std::endl;
    }

    /*perform chosen operation*/
    switch (dbMenu.getOperationChoice())
    {
    case 1: //select/read
        selectHandler();
        break;
    case 2: //insert/add
        insertHandler();
        break;
    case 3: //update/change
        updateHandler();    
        break;
    case 4: //delete/remove
        deleteHandler();
        break; 
    default:
        break;
    }

}

int DataBaseHandler::selectHandler()
{
    dbInterface.listTables();
    dbMenu.chooseTable();
    while(dbInterface.CheckTable(dbMenu.getTableChoice())== 0) //not existst
    {
        dbMenu.chooseTable();
        std::cout << "non-valid value, try again" << std::endl;
    }
    
    dbInterface.selectRecord(dbMenu.getTableChoice());
    std::cin.get();
    std::cout << "Press a key to conitinue" << std::endl;
    std::cin.get();  
    std::system("clear");

    return 0;  
}

int DataBaseHandler::insertHandler()
{
    dbInterface.listTables();
    dbMenu.chooseTable();
    while(dbInterface.CheckTable(dbMenu.getTableChoice())== 0) //not existst
    {
        dbMenu.chooseTable();
        std::cout << "non-valid value, try again" << std::endl;
    }
    
    dbInterface.listattributes(dbMenu.getTableChoice());
    dbMenu.chooseAttribute();
    while(dbInterface.CheckAttribute(dbMenu.getTableChoice(), dbMenu.getAttribute()) == 1)
    {
        dbMenu.chooseAttribute();
        std::cout << "non-valid value, try again" << std::endl;
    }

    dbMenu.fillInVal();
    dbInterface.insertRecord(dbMenu.getTableChoice(), dbMenu.getAttribute(), dbMenu.getValue());

    std::cin.get();
    std::cout << "Press a key to continue" << std::endl;    
    std::cin.get();
    std::system("clear");

    return 0;  
}

int DataBaseHandler::updateHandler()
{
    dbInterface.listTables();
    dbMenu.chooseTable();
    while(dbInterface.CheckTable(dbMenu.getTableChoice())== 0) //not existst
    {
        dbMenu.chooseTable();
        std::cout << "non-valid value, try again" << std::endl;
    }

    dbInterface.listattributes(dbMenu.getTableChoice());
    dbMenu.chooseAttribute();
    while(dbInterface.CheckAttribute(dbMenu.getTableChoice(), dbMenu.getAttribute()) == 1)
    {
        dbMenu.chooseAttribute();
        std::cout << "non-valid value, try again" << std::endl;
    }

    dbInterface.listattributes(dbMenu.getTableChoice());
    dbMenu.choosePrimKey();
    while(dbInterface.CheckAttribute(dbMenu.getTableChoice(), dbMenu.getPrimKey()) == 1)
    {
        dbMenu.choosePrimKey();
        std::cout << "non-valid value, try again" << std::endl;
    }

    dbInterface.selectRecord(dbMenu.getTableChoice());
    dbMenu.choosePrimKeyVal();

    dbMenu.fillInVal();
    dbInterface.updateRecord(dbMenu.getTableChoice(),dbMenu.getAttribute(),dbMenu.getValue(),dbMenu.getPrimKey(),dbMenu.getPrimKeyValue());

    std::cin.get();
    std::cout << "Press a key to continue" << std::endl;    
    std::cin.get();
    std::system("clear"); 

    return 0;  
}

int DataBaseHandler::deleteHandler()
{
    dbInterface.listTables();
    dbMenu.chooseTable();
    while(dbInterface.CheckTable(dbMenu.getTableChoice())== 0) //not existst
    {
        dbMenu.chooseTable();
        std::cout << "non-valid value, try again" << std::endl;
    }

    dbInterface.listattributes(dbMenu.getTableChoice());
    dbMenu.choosePrimKey();
    while(dbInterface.CheckAttribute(dbMenu.getTableChoice(), dbMenu.getPrimKey()) == 1)
    {
        dbMenu.choosePrimKey();
        std::cout << "non-valid value, try again" << std::endl;
    }

    dbInterface.selectRecord(dbMenu.getTableChoice());
    dbMenu.choosePrimKeyVal();

    dbInterface.deleteRecord(dbMenu.getTableChoice(), dbMenu.getPrimKey(), dbMenu.getPrimKeyValue());

    std::cin.get();
    std::cout << "Press a key to continue" << std::endl;
    std::cin.get(); 
    std::system("clear");


    return 0;  
}
