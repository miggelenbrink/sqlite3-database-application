#ifndef menuDataBase_H
#define menuDataBase_H

    /*examples of database interface functions*/  
    /*
    interfaceDataBase testDB("test.db");
    testDB.listTables();
    testDB.selectRecord("employees");
    testDB.insertRecord("employees", "name ","'jelle m'");
    testDB.updateRecord("employees","name","'johan m'","id","4");
    testDB.deleteRecord("employees", "id", "3");
    */

#include <iostream>
#include <string>

class menuDataBase
{
public:
    menuDataBase();
    ~menuDataBase();

    int selectOperation();
    void chooseTable();
    void fillInVal();
    void choosePrimKey();
    void choosePrimKeyVal();
    std::string getInput();
    void clearscreen();
    void chooseAttribute();

    //getters
    std::string getTableChoice() const;
    int getOperationChoice() const;
    std::string getValue() const;
    std::string getPrimKey() const;
    std::string getPrimKeyValue() const;
    std::string getAttribute() const;


private:

    std::string tableChoice;
    int OperationChoice;
    std::string value;
    std::string PrimKey;
    std::string PrimKeyValue;
    std::string attribute;

};

#endif //menuDataBase_H