#include "interfaceDataBase.h"
#include <iostream>
#include <stdio.h>

static int callback(void *data, int argc, char **argv, char **azColName) {

    for (int i = 0; i < argc; i++)
        std::cout << azColName[i] << " = " << (argv[i] ? argv[i] : "NULL") << "\n";

    std::cout << std::string(50, '-') << "\n";
    return 0;
}

interfaceDataBase::interfaceDataBase(const std::string name) : name_db{name}
{
   int rc = sqlite3_open(name_db.c_str(), &db);

   if(rc){
       std::clog << "failed to open database: " << name_db << " :%s\n" << sqlite3_errmsg(db) << " \n" << std::endl;
   }
   else{
       std::clog << "succesfully opened database: " << name_db << " \n" << std::endl;
   }
   
}

interfaceDataBase::~interfaceDataBase()
{
    int rc = sqlite3_close(db);
    
    if(rc == SQLITE_OK){
        std::clog << "succesfully closed database: " << name_db << " \n" << std::endl;
    }
    else{
        std::clog << "failed to close database: " << name_db << " :%s\n" << sqlite3_errmsg(db) << " \n" << std::endl;
    }
}

int interfaceDataBase::insertRecord(const std::string tableName, const std::string attribute, const std::string value)
{
    std::cout << "inserted " << value << " in table " << tableName << "\n" << std::endl;

    /* create SQL statement */
    sql = "INSERT INTO " + tableName + "(" + attribute +  ") " + "VALUES" + "(" + value +  ") " + "; ";
    
    /* Execute SQL statement */
    const char *data = "empty";
    int rc = sqlite3_exec(db, sql.c_str(), nullptr, (void*)data, &errMsg);

    /* Check returnvalue */
    if(rc == SQLITE_OK){
        //std::clog << "succesfully created record " << " \n" << std::endl;
        return 0;
    }
    else{
        std::clog << "failed to create record " << " \n" << std::endl;
        return 1;
    }
}

int interfaceDataBase::selectRecord(const std::string tableName)
{
    std::system("clear");
    std::cout << "records from " << tableName << " table:\n" <<std::endl;

    /* create SQL statement */
    sql = "SELECT * from " + tableName;
            
    /* Execute SQL statement */
    const char *data = "empty msg";
    int rc = sqlite3_exec(db, sql.c_str(), callback, (void*)data, &errMsg);

    /* Check returnvalue */
    if(rc == SQLITE_OK){
        //std::clog << "succesfully selected table \n " << std::endl;
        return 0;
    }
    else{
        std::clog << "failed to select table \n" << std::endl;
        return 1;
    }
}

int interfaceDataBase::updateRecord(const std::string table, const std::string attribute, const std::string value, const std::string primKeyName, const std::string id)
{
    //std::cout << "update a record in table " << table << " with value " << value << "\n" <<std::endl;

    /* create SQL statement */
    sql = "UPDATE " + table + " set " + attribute + " = " + value + " where " + primKeyName + "=" + id + ";";
            
    /* Execute SQL statement */
    const char *data = "empty";
    int rc = sqlite3_exec(db, sql.c_str(), nullptr, (void*)data, &errMsg);

    /* Check returnvalue */
    if(rc == SQLITE_OK){
        std::clog << "succesfully updated record " << " \n" << std::endl;
        return 0;
    }
    else{
        std::clog << "failed to update record " << std::endl;
        std::cout << "update failed because of wrong or non existing primerykey/id\n" << std::endl;
        return 1;
    }
}

int interfaceDataBase::deleteRecord(const std::string table, const std::string primKeyName, const std::string id)
{
    //std::cout << "delete record in table " << table << "\n" << std::endl;

    /* create SQL statement */
    sql = "DELETE from " + table + " where " + primKeyName + "=" + id + ";";
            
    /* Execute SQL statement */
    const char *data = "empty";
    int rc = sqlite3_exec(db, sql.c_str(), nullptr, (void*)data, &errMsg);

    /* Check returnvalue */
    if(rc == SQLITE_OK){
        std::clog << "succesfully deleted record " << " \n" << std::endl;
        return 0;
    }
    else{
        std::clog << "failed to delete record " << " \n" << std::endl;
        return 1;
    }
}

int interfaceDataBase::listTables()
{
    std::cout << "list of existing tables in database:\n" <<std::endl;

    /* create SQL statement */
    sql = "SELECT * FROM sqlite_master where type='table'";
            
    /* Execute SQL statement */
    const char *data = "empty";
    int rc = sqlite3_exec(db, sql.c_str(), callback, (void*)data, &errMsg);

    /* Check returnvalue */
    if(rc == SQLITE_OK){
        //std::clog << "succesfully listed tables " << " \n" << std::endl;
        return 0;
    }
    else{
        std::clog << "failed to list tables " << " \n" << std::endl;
        return 1;
    }
}

int interfaceDataBase::listattributes(const std::string attribute)
{
    std::cout << "list of existing attributes in table:\n" <<std::endl;

    /* create SQL statement */
    sql = "SELECT name FROM PRAGMA_TABLE_INFO('" + attribute + "')";
            
    /* Execute SQL statement */
    const char *data = "empty";
    int rc = sqlite3_exec(db, sql.c_str(), callback, (void*)data, &errMsg);

    /* Check returnvalue */
    if(rc == SQLITE_OK){
        //std::clog << "succesfully listed attributes " << " \n" << std::endl;
        return 0;
    }
    else{
        std::clog << "failed to list attributes " << " \n" << std::endl;
        return 1;
    }
}

int interfaceDataBase::CheckTable(const std::string name)
{
    //std::cout << "Check if "<< name << " table exists in database:\n" << std::endl;  

    sqlite3_stmt *stmt;
    std::string sql = "SELECT 1 FROM sqlite_master where type='table' and name='" + name + "'";

    int rc = sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
      std::cout << "error: " << sqlite3_errmsg(db) << std::endl;
    }
    sqlite3_bind_text(stmt, 1, name.c_str(), -1, SQLITE_TRANSIENT);

    rc = sqlite3_step(stmt);
    bool found;
    if (rc == SQLITE_ROW)
    {
       found = true;
       //std::cout << "found table\n" << std::endl;
    }
    else if (rc == SQLITE_DONE)
    {
        found = false;
        std::cout << "table not found\n" << std::endl;
    }
    else {
        std::cout << "error: " << sqlite3_errmsg(db) << std::endl;
        sqlite3_finalize(stmt);
    }   
    sqlite3_finalize(stmt);
    return found;
}


int interfaceDataBase::CheckAttribute(const std::string name, const std::string attribute)
{
    //std::cout << "Check if "<< attribute << " attribute exists in" << name << "\n" << std::endl;  

    /* create SQL statement */
    sql = "SELECT " + attribute + " FROM " + name;
        
    
    /* Execute SQL statement */
    const char *data = "empty";
    int rc = sqlite3_exec(db, sql.c_str(), nullptr, (void*)data, &errMsg);

    /* Check returnvalue */
    if(rc == SQLITE_OK){
        //std::clog << "attribute/column exists" << " \n" << std::endl;
        return 0;
    }
    else{
        std::clog << "attribute/column does not exist " << " \n" << std::endl;
        return 1;
    }
}

