#include "menuDataBase.h"
#include <limits>
#include <iostream>
#include <stdlib.h>
#include <cstdlib>

menuDataBase::menuDataBase(): tableChoice{}, OperationChoice{}, value{}, PrimKey{}, PrimKeyValue{}
{
}

menuDataBase::~menuDataBase()
{
}

int menuDataBase::selectOperation()
{
    std::cout << "Select an operation to perform on a table: \n\n";

    std::cout << "1. Select table \n";
    std::cout << "2. Insert record\n";
    std::cout << "3. Update record\n";
    std::cout << "4. Delete record\n\n";
    std::cout << "press ctrl + c to exit\n\n";

    OperationChoice = std::stoi( getInput() );
    if(OperationChoice == 1 | 2 | 3 | 4)
    {
        return 0;
    }
    else
    {
        std::cout << "choose an existing operation" << std::endl;
        return 1;
    }
    
}

void menuDataBase::chooseTable()
{
    std::cout << "Choose a table from the databases table list.\n" << std::endl;
    tableChoice = getInput();
}

void menuDataBase::fillInVal()
{
    std::cout << "Enter value to insert" << std::endl;
    std::cout << "keep in mind, when inserting string to include ' ' \n" << std::endl;

    value = getInput();
}

void menuDataBase::choosePrimKey()
{
    std::cout << "Enter id name\n" << std::endl;
    PrimKey = getInput();
}

void menuDataBase::choosePrimKeyVal()
{
    std::cout << "Enter id\n" << std::endl;
    PrimKeyValue = getInput();
}


std::string menuDataBase::getInput()
{
    std::string input;
    std::cin >> input ;
    if (std::cin.fail()) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        input = nullptr;
    }
    // clear screen;
    std::system("clear");
    return input;
}

void menuDataBase::chooseAttribute()
{
    std::cout << "Enter attribute\n" << std::endl;
    attribute = getInput();
}

std::string menuDataBase::getAttribute() const
{
    return attribute;
}

void menuDataBase::clearscreen()
{
    std::system("clear");
}

    std::string menuDataBase::getTableChoice() const
    {
        return tableChoice;
    }

    int menuDataBase::getOperationChoice() const
    {
        return OperationChoice;
    }

    std::string menuDataBase::getValue() const
    {
        return value;
    }
    
    std::string menuDataBase::getPrimKey() const
    {
        return PrimKey;
    }

    std::string menuDataBase::getPrimKeyValue() const
    {
        return PrimKeyValue;
    }
