#ifndef interfaceDataBase_H
#define interfaceDataBase_H

#include <sqlite3.h>
#include <string>

class interfaceDataBase
{    
public:
    /** constructor opens or creates a database.
     * 
     *  @param name name database file to open
     */
    interfaceDataBase(const std::string name);

    /** deconstructor closes database.
     * 
     */
    ~interfaceDataBase();

    /** create records in a table.
     *  
     *  @param tableName table name
     *  @param values value to insert in table
     *  @return return 1 when failed to insert, else 0.
     */
    int insertRecord(const std::string tableName, const std::string attribute, const std::string value);

    /** fetch all records form table.
     *  
     *  @param tableName table name
     *  @return return 1 when failed to insert, else 0.
     */
    int selectRecord(const std::string tableName);

    /** update a record in table.
     *  
     *  @param table table name to work on
     *  @param attribute attribute name
     *  @param value value to replace current record value
     *  @param primKeyName name of primeray keys used in specefic table
     *  @param id id value of record to update 
     *  @return return 1 when failed to insert, else 0.
     */
    int updateRecord(const std::string table, const std::string attribute, const std::string value, const std::string primKeyName, const std::string id);

    /** delete records from table.
     *  
     *  @param table table name to work on
     *  @param primKeyName name of primeray keys used in specefic table
     *  @param id id value of record to update 
     *  @return return 1 when failed to insert, else 0.
     */
    int deleteRecord(const std::string table, const std::string primKeyName, const std::string id);

    /** fetch all tables.
     *  
     *  @param 
     *  @return return 1 when failed to insert, else 0.
     */
    int listTables();

    /** check if table exists.
     *  
     *  @param 
     *  @return return 0 if table does not exists, 1 if it does.
     */
    int CheckTable(const std::string name);    

    /** check if attribute exists.
     *  
     *  @param 
     *  @return return 0 if attribute does not exists, 1 if it does.
     */
    int CheckAttribute(const std::string name, const std::string attribute); 

    int listattributes(const std::string attribute);

    
private:
    sqlite3 *db;            ///< database object
    std::string name_db;    ///< database name
    char *errMsg;           ///< error message sqlite3 functions
    std::string sql;        ///< sql statements

};


#endif //interfaceDataBase_H