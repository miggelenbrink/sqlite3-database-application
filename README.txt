It is preffered to open this project in gitlab:
https://gitlab.com/miggelenbrink/sqlite3-database-application

## Application to interface with a database.

**Before using the application:**

- Make sure that in the main.cpp an existing database name is passed to the DataBase handler object.  

  `DataBaseHandler SchoolManagementsDB{"SchoolManagmentSystem.db"};`  

- Compile  
    
    In the root folder execute the `make` command.  
    This wil create the executable file.  


- execute  
    
    Enter in the root folder the command:   
    `./dbInterface`  
    This will run the application  

	
## SchoolManagementSystem database

The application can be used with any database.  
But for the assesment there had to be designed and created a database.

Therefor I chose to design and create a database for schools.
To be more specific the database is called SchoolManagementSystem. 
In the schoolManagementSystem data regarding school, students, teachers and coursed can be stored.  
For the detailed database design look into the documentation folder, there is a PDF file with the design schemas. 
From the schemas I made SQLITE statements, which are also in the documentation folder.
The statements are used in a terminal to create the actual SchoolManagementSystem database. 

The database created with the SQLITE statemetns is in the root folder and  
is as default used by the database application.


